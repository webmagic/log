<?php

namespace Webmagic\Log;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;
use Rap2hpoutre\LaravelLogViewer\LaravelLogViewerServiceProvider;
use Webmagic\Dashboard\Dashboard;

class LogDashboardServiceProvider extends ServiceProvider
{
    /**
     * Register log services
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/config/log_dashboard.php', 'webmagic.dashboard.log'
        );

        $this->registerServiceProviders();
    }

    /**
     * Register routes
     * @param Router $router
     */
    public function boot(Router $router)
    {
        //Routs registering
        $this->loadRoutesFrom(__DIR__ . '/Http/routes.php');

        //Load Views
        $this->loadViewsFrom(__DIR__.'/resources/views', 'log');

        $this->loadTranslations();

        //Config publishing
        $this->publishes([
            __DIR__.'/config/log_dashboard.php' => config_path('webmagic/dashboard/log.php'),
        ], 'config');


        //Add items menu
        $this->includeMenuForDashboard();

        $this->registeringMiddleware($router);
    }

    /**
     * @return array
     */
    protected function getMenuItemConfig(): array
    {
        return [
            'link' => 'dashboard/options/log',
            'text' => 'log::common.log',
            'icon' => 'fa-file',
            'active_rules' => [
                'routes_parts' => [
                    'dashboard/options/log',
                ],
            ],
        ];
    }

    /**
     * Including menu items for new dashboard
     */
    protected function includeMenuForDashboard()
    {
        if(config('webmagic.dashboard.log.show_log_menu_item')){
            $dashboard = $this->app->make(Dashboard::class);
            $dashboard->getMainMenu()->addMenuItems($this->getMenuItemConfig());
        }
    }


    /**
     * Load translation
     */
    protected function loadTranslations()
    {
        $this->loadTranslationsFrom(__DIR__.'/resources/lang', 'log');
    }

    /**
     * Register service providers
     */
    public function registerServiceProviders()
    {
//        $this->app->register('Rap2hpoutre\LaravelLogViewer\LaravelLogViewerServiceProvider');
//        $this->app->register('\Webmagic\Log\LogEventServiceProvider');
        $this->app->singleton(LaravelLogViewerServiceProvider::class, LaravelLogViewerServiceProvider::class);
        $this->app->singleton(LogEventServiceProvider::class, LogEventServiceProvider::class);
    }


    /**
     * Middleware registration
     *
     * @param $router
     */
    public function registeringMiddleware($router)
    {
        $router->middlewareGroup('log', [
            \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
            \Illuminate\Cookie\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \Illuminate\Foundation\Http\Middleware\VerifyCsrfToken::class,
        ]);
    }
}
